<?
class translater {
    private $domain = 'https://translate.google.com/translate_a/single?client=webapp';
    
    // private $domain = 'https://www.webtran.ru/gtranslate/';
    // http://joxi.ru/KAg49QEHX1xw4r
    
    // http://tetran.ru/OnLineTranslator
    // http://joxi.ru/gmv4ZkjHv7EYjA
    
    // https://ru.translatoro.com/processor.xhr
    // http://joxi.ru/xAeqlP8Ug3nzpA
    
    function __construct($options) {
        $this->from_lang = $options['FROM_LANG'];
        $this->to_lang = $options['TO_LANG'];
        $this->charset = $options['CHARSET'];
    }

    private function xr($a, $b) {
        for ($i = 0; $i < strlen($b) - 2; $i += 3) {
            $d = $b[$i + 2];
            $d = $d >= 'a' ? (mb_ord($d[0]) - 87) : $d;
            $d = '+' == $b[$i + 1] ? ($a >> $d) : ($a << $d);
            $a = '+' == $b[$i] ? ($a + $d & 4294967295) : ($a ^ $d);
        }
        return $a;
    }

    private function generateTK($str = '') {
        $TKK = '442066.2984557923'; // window['TKK']
    
        $urlParam = '&tk=';
        $TKK = explode('.', $TKK);
        $TKK_0 = $TKK[0];
        $TKK_1 = $TKK[1];
    
        $e = array();
    
        for ($e = [], $f = 0, $g = 0; $g < strlen($str); $g++) {
            $l = mb_ord($str[$g]);
            if (128 > $l) {
                $e[] = $l;  
            }
            else
            {
                if(2048 > $l)
                {
                    $e[] = $l >> 6 | 192 ;
                }
                else
                {
                    if(55296 == ($l & 64512) && $g + 1 < strlen($str) && 56320 == (mb_ord($str[$g + 1]) & 64512))
                    {
                        $l = 65536 + (($l & 1023) << 10) + (mb_ord($str[++$g]) & 1023);
                        $e[] = $l >> 18 | 240;
                        $e[] = $l >> 12 & 63 | 128;
                    }
                    else
                    {
                        $e[] = $l >> 12 | 224;
                        $e[] = $l >> 6 & 63 | 128;
                    }
                }
                $e[] = $l & 63 | 128;
            }
        }
    
    
        for($ff = 0; $ff < count($e); $ff++)
        {
            $TKK_0+=$e[$ff];
            $TKK_0 = $this->xr($TKK_0, "+-a^+6");
        }
    
        $TKK_0 = $this->xr($TKK_0, "+-3^+b+-f");
        $TKK_0 ^= $TKK_1;
        $bigA = ($TKK_0 & 2147483647+2147483648);
        $bigA %= 1E6;
    
        $result = $urlParam.($bigA.".".($bigA ^ $TKK[0]));
    
        return $result;
    }

    private function encode($str) {
        if(strtoupper($this->charset) != 'UTF-8') {
            $str = iconv($this->charset, 'UTF-8', $str);
        }
        
        return urlencode( $str );
    }

    private function generateUrl($str) {
        $url = $this->domain.'&sl='.$this->from_lang.'&tl='.$this->to_lang.'&dt=t'.$this->generateTK($str).'&q='.$this->encode($str);
        return $url;
    }

    public function translate($str) {
        $content = file_get_contents( $this->generateUrl($str) );
        $content = json_decode($content, true);
        return $content[0][0][0];
    }
}

$options = array(
	'FROM_LANG' => 'ru',
	'TO_LANG' => 'en',
	'CHARSET' => 'windows-1251',
);
$trans = new translater($options);


$result = $trans->translate('Тестирование приложения');

print_r($result);

?>