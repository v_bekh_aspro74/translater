<?
$pathToLangFile = $_SERVER['DOCUMENT_ROOT']."/bitrix/templates/aspro-priority/lang/ru/header.php";
$contentLangRu = file_get_contents($pathToLangFile);

$pattern = '/\$MESS\[[\'\"](.*?)[\'\"]\]\s*=\s*[\'\"](.*?)[\'\"]\;/s';
preg_match_all($pattern, $contentLangRu, $arLangMessages);

if($arLangMessages[2]) {
	$arLangEn = array();
	foreach($arLangMessages[2] as $messKey => $mess){
		$result = $trans->translate($mess);
		$arLangEn[ $arLangMessages[1][$messKey] ] = $result;
	}
	$sLangFileText = '<?'.PHP_EOL; 
	foreach($arLangEn as $messKey => $mess){
		$sLangFileText .= '$MESS["'.$messKey.'"] = "'.$mess.'";'.PHP_EOL;
	}
	$sLangFileText .= '?>'; 
}

print_r($sLangFileText);
?>